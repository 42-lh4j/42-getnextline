# get_next_line 

![cover](/assets/GET%20NEXT%20LINE.png)

### Cases To Check !

- case 1: passing a file that doesn't exist .

- case 2: passing a file with no content .

- case 3: passing a file with content that ends with:
1. a new line !?
2. no new line !?

### Objective:
  - write a function that returns a line read from a file descriptor
  - learn about static variables and how do they operates under the hood
  - get to know file descriptors and how to duplicate it !

### Instructions:
  - repeated calls to get_next_line would allow you to read 
    the text file pointed towards the file descriptor !
  - the function should return the line that was read !
  - in case of an error or nothing to read return NULL .
  - make sure the function works as expected when 
    reading from a file or from the standard input !
  - **NOTE**: the returned line should include a terminating '\n'
          except if the end of file was reached 
          and it does not end with a '\n'
  - **WARNING**:  we must be able to compile the project with the
              -D BUFFER_SIZE flag or without it 
              (choose a default value of you're choice) !
